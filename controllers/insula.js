const InsulaDb = require("../models").Insula;
const CrocodilDb = require("../models").Crocodil;

const controller = {
  getAllIslands: async (req, res) => {
    InsulaDb.findAll()
      .then((islands) => {
        res.status(200).send(islands);
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: "Server error!" });
      });
  },

  getIslandById: async (req, res) => {
    const { id } = req.params;
    if (!id) {
      res.status(400).send({ message: "ID not provided!" });
    }

    InsulaDb.findByPk(id)
      .then((insula) => {
        res.status(200).send(insula);
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: "Server error!" });
      });
  },

  getCrocodilesFromIsland: async (req, res) => {
    const { id } = req.params;
    // aici vezi ca nu face verificarea, dacva nu i dai id se duce total pe alta ruta, pe aia de get insula by id nush dc plm
    if (!id) {
      res.status(400).send({ message: "ID not provided!" });
    }
    console.log(id);

    InsulaDb.findByPk(id, {
      include: [{ model: CrocodilDb, as: "Crocodil" }],
    })
      .then((insula) => {
        res.status(200).send(insula);
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: "Server error!" });
      });
  },

  addCrocodile: async (req, res) => {
    const { id, nume, prenume, varsta } = req.body;
    InsulaDb.findByPk(id)
      .then((insula) => {
        if (insula) {
          console.log(insula);
          insula
            .createCrocodil({ nume, prenume, varsta })
            .then((crocodil) => {
              res.status(201).send(crocodil);
            })
            .catch((err) => {
              console.log(err);
              res.status(500).send({ message: "Server error!" });
            });
        } else {
          res.status(404).send({ message: "Insula not found!" });
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: "Server error!" });
      });
  },
};

module.exports = controller;
