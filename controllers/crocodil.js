const CrocodilDb = require("../models").Crocodil;

const controller = {
  getAllCrocodiles: async (req, res) => {
    CrocodilDb.findAll()
      .then((crocodili) => {
        res.status(200).send(crocodili);
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: "Server error!" });
      });
  },

  
};

module.exports = controller;
