const express = require("express");
const router = require("./routes");
const connection = require("./models").connection;
const app = express();

app.use(express.json());

let port = 8081;

app.use("/api", router);

app.get("/reset", (req, res) => {
  connection
    .sync({ force: true })
    .then(() => {
      res.status(201).send({
        message: "Database reset",
      });
    })
    .catch(() => {
      res.status(500).send({
        message: " Reset DB error",
      });
    });
});

app.use("/*", (req, res) => {
  res.status(200).send("Rulez boss");
});

app.listen(port, () => {
  console.log("Server is running on " + port);
});
