const express = require("express");
const router = express.Router();
const insulaController = require("../controllers").insula;

router.post("/", insulaController.addCrocodile);
router.get("/:id/crocodili", insulaController.getCrocodilesFromIsland);
router.get("/", insulaController.getAllIslands);
router.get("/:id", insulaController.getIslandById);

module.exports = router;
