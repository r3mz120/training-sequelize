const express = require("express");
const router = express.Router();
const crocodilController = require("../controllers").crocodil;

router.get("/", crocodilController.getAllCrocodiles);

module.exports = router;
