const express = require("express");
const router = express.Router();
const insuleRouter = require("./insula");
const crocodilRouter = require("./crocodil");

router.use("/insule", insuleRouter);
router.use("/crocodili", crocodilRouter);

module.exports = router;
